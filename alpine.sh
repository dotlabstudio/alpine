#!/usr/bin/env bash

# install some base packages for alpine

doas apk add neofetch util-linux pciutils usbutils coreutils binutils findutils grep iproute2 udisks2 udisks2-doc build-base gcc abuild binutils binutils-doc gcc-doc cmake cmake-doc extra-cmake-modules extra-cmake-modules-doc ccache ccache-doc

# setup flatpaks on alpine
doas apk add flatpak

# setup flathub
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# sources: https://wiki.alpinelinux.org/wiki/How_to_get_regular_stuff_working
#          https://wiki.alpinelinux.org/wiki/Flatpak

# extra stuff needed for a alpine system
doas apk add xz shadow

echo "Finish setting some base packages for Alpine Linux"
