#!/usr/bin/env bash

# Install tailscale
doas apk add tailscale

# Use OpenRC to enable and start the service
doas rc-update add tailscale
doas rc-service tailscale start

# Authenticate and connect your machine to your Tailscale network
doas tailscale up
