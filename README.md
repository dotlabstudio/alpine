# Alpine Linux as a Desktop Project

This repo shows how to setup alpine linux as a desktop.

# Grab the ISO from the home page

https://www.alpinelinux.org/

# Install Process

Login as root without password to use

```
setup-alpine
```

use cryptsys for a laptop

use the first default mirror

# Post Install

Add some basic packages for editing stuff:

```
doas apk add vim fish htop git
```

## Edit the repos

```
doas vim /etc/apk/repositories
```

comment all the existing repos and append these lines:

```
http://dl-cdn.alpinelinux.org/alpine/latest-stable/main
http://dl-cdn.alpinelinux.org/alpine/latest-stable/community
```

## Update the system after repositories changes

```
doas apk update

doas apk add --upgrade apk-tools

doas apk upgrade --available

doas sync

doas reboot
```

## Lock the root account

```
doas passwd -l root
```

# Setup the desktop

```
doas setup-desktop

doas apk add kde-applications
```

select plasma

## Add apparmor

```
doas apk add apparmor apparmor-utils apparmor-profiles
```

### check security modules

```
cat /sys/kernel/security/lsm
```

### Edit With GRUB

Add the following at the end of the value for key GRUB_CMDLINE_LINUX_DEFAULT to /etc/default/grub:

```
apparmor=1 security=apparmor
```

Then apply with:

```
doas grub-mkconfig -o /boot/grub/grub.cfg
```

### Enable apparmor on boot

```
doas rc-service apparmor start

doas rc-update add apparmor boot
```

reboot the system

```
doas reboot
```

after the reboot
```
doas aa-enforce /etc/apparmor.d/*
```

see https://wiki.alpinelinux.org/wiki/AppArmor

# Setup Tailscale

## Install tailscale
```
doas apk add tailscale
```

## Use OpenRC to enable and start the service
```
doas rc-update add tailscale
doas rc-service tailscale start
```

## Authenticate and connect your machine to your Tailscale network
```
doas tailscale up
```

# Updating the system in Alpine

```
doas apk update

doas apk upgrade
```

or

```
doas apk -U upgrade
```

see https://wiki.alpinelinux.org/wiki/Alpine_Package_Keeper

# Setup virt manager

```
doas apk add libvirt-daemon qemu-img qemu-system-x86_64 qemu-modules openrc dbus polkit virt-manager font-terminus

doas rc-update add libvirtd

doas rc-update add libvirt-guests

doas rc-update add dbus
```

## add user to qemu, kvm and libvirt groups

```
doas addgroup $USER qemu

doas addgroup $USER kvm

doas addgroup $USER libvirt

```

see https://wiki.alpinelinux.org/wiki/KVM


