#!/usr/bin/env bash

# Install some fonts on Alpine

doas apk add font-terminus font-inconsolata font-dejavu font-noto font-noto-cjk font-awesome font-noto-extra

# for cyrillic languages like Russian and Serbian, etc.:

doas apk add font-vollkorn font-misc-cyrillic font-mutt-misc font-screen-cyrillic font-winitzki-cyrillic font-cronyx-cyrillic

# for Asiatic languages like Japanese, etc.:

doas apk add font-terminus font-noto font-noto-thai font-noto-tibetan font-ipa font-sony-misc font-daewoo-misc font-jis-misc

# for partially supported Chinese fonts:

doas apk add font-isas-misc

# for Arabic, Thai, Ethiopic, Hebrew, Romanian, Persian, Korean Hangul, Greek, Persian, Russian/Slavic Cyrillic, Macedonian/Serbian, Armenian, Georgian, Lao, Devanagari, Urdu (Hindustani as in Northern India and Pakistan), Cherokee, Thaana languages support for desktop setups:

doas apk add font-terminus font-bitstream-* font-noto font-noto-extra font-arabic-misc 

doas apk add font-misc-cyrillic font-mutt-misc font-screen-cyrillic font-winitzki-cyrillic font-cronyx-cyrillic

doas apk add font-noto-arabic font-noto-armenian font-noto-cherokee font-noto-devanagari font-noto-ethiopic font-noto-georgian

doas apk add font-noto-hebrew font-noto-lao font-noto-malayalam font-noto-tamil font-noto-thaana font-noto-thai

# install core fonts

doas apk add msttcorefonts

# update fonts cache
fc-cache -fv

# sources : https://wiki.alpinelinux.org/wiki/Fonts
