#!/usr/bin/env bash

# This script installs plasma and services on an alpine system

doas apk add plasma elogind polkit-elogind

# install plasma apps
doas apk add kde-applications

# enable services
doas rc-update add dbus

doas rc-update add elogind

doas rc-update add polkit

# setup sddm

doas rc-update add sddm

doas rc-service sddm start

# add user to pipewire group
doas addgroup $USER pipewire

# source: https://wiki.alpinelinux.org/wiki/KDE

echo "Please reboot the system with doas reboot"

