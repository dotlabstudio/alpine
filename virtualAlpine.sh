#!/usr/bin/env bash

# install virt manager stuff
doas apk add libvirt-daemon qemu-img qemu-system-x86_64 qemu-modules openrc dbus polkit virt-manager font-terminus

# enable virtual machine services
doas rc-update add libvirtd

doas rc-update add libvirt-guests

doas rc-update add dbus

# add user to virtual machine groups
doas addgroup $USER qemu

doas addgroup $USER kvm

doas addgroup $USER libvirt

echo "Finished setting up virt manager"

